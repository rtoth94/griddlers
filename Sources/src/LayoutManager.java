import javafx.scene.layout.BorderPane;
import java.io.IOException;

/**
 * This class is responsible for positioning the layouts
 */

class LayoutManager extends BorderPane {

    private double w = 100, h = 500;


    LayoutManager() throws IOException {
        setPrefSize(w, h);
        setTop(new TopLayout());
        setBottom(new BottomLayout());
        setLeft(new LeftLayout());
        setCenter(new Game());

    }


    void setH(double h) {
        this.h = h;
    }

    void setW(double w) {
        this.w = w;
    }


}
