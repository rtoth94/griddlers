
import java.io.*;

/**
 * This class is responsible for reading the given .txt file.
 */
class FileRead {

    private char[][] map;
    private boolean allocated = false;
    private String path;
    int innerLength = 0;
    int outerLength = 0;


    /**
     *
     * @param p Filepath as string
     * @throws IOException File doesn't exists
     */
    FileRead(String p) throws IOException {
        path = p;
        Read();
    }


    private void Read() throws IOException {
        BufferedReader br = getBufferedReader(path);
        String line;
        int index = 0;


        while ((line = br.readLine()) != null) {
            map = allocate();
            map[index] = line.toCharArray();
            innerLength = line.toCharArray().length;
            index++;
        }


    }

    /**
     * Get the file length
     * @return returns length of file (lines) as Integer
     * @throws IOException ""
     */
    private int GetLength() throws IOException {
        BufferedReader br = getBufferedReader(path);
        int file_length = 0;
        while (br.readLine() != null) {
            file_length++;
        }
        return file_length;
    }


    /**
     * Create and return Buffered reader
     * @param filename Filepath as String
     * @return Buffered reader
     * @throws FileNotFoundException ""
     */
    private BufferedReader getBufferedReader(String filename) throws FileNotFoundException {
        File file = new File(filename);
        FileReader freader = new FileReader(file);
        return new BufferedReader(freader);
    }


    /**
     * Allocate the charmap
     * @return Charmap, 2D array of characters in file.
     * @throws IOException ""
     */
    private char[][] allocate() throws IOException {
        if (!allocated) {
            int outer_size = GetLength();
            outerLength = outer_size;
            map = new char[outer_size][];
            allocated = true;
        }
        return map;
    }


    char[][] GetCharMap() {
        return map;
    }


}