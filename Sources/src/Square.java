import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

/**
 * This class is represents a clickable square object on a plazground.
 * It stores the current value and the final value too (solution)
 */
public class Square extends Rectangle implements Serializable {


    protected int final_value;
    int current_value;
    private int indexi;
    private int indexj;
    private int offset;
    private Map<Integer, Character> homomorf = new HashMap<>();
    boolean click;


    Square(int i, int j, int current_value, int final_value) {
        this.indexi = i;
        this.indexj = j;
        this.current_value = current_value;
        this.final_value = final_value;
        homomorf.put(1, 'X');
        homomorf.put(0, '.');
        homomorf.put(2, 'O');
        offset = 140;
        click = false;

        SetEvent();
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(homomorf.get(current_value));
        return sb.toString();
    }

    /**
     * Acording to current value of square choose a color
     * @param fin if we check the final value instead of current value
     * @return returns a color
     */
    Color ColorSelector(boolean fin) {
        Color res;
        int x;
        if (fin) {
            x = final_value;
        } else {
            x = current_value;
        }

        switch (x) {
            case 0:
                res = Color.WHITE;
                break;
            case 1:
                res = Color.RED;
                break;
            case 2:
                res = Color.GRAY;
                break;
            default:
                res = Color.WHITE;
                break;
        }

        return res;
    }

    Rectangle DrawFinal() {
        setWidth(25);
        setHeight(25);
        setFill(ColorSelector(true));
        setStroke(Color.WHITE);
        setStrokeWidth(1);
        return this;
    }

    Rectangle DrawLoaded() {
        setWidth(25);
        setHeight(25);
        setFill(ColorSelector(false));
        setStroke(Color.GREEN);
        setStroke(Color.WHITE);
        setStrokeWidth(1);
        return this;
    }

    Rectangle DrawS() {
        setWidth(25);
        setHeight(25);
        setFill(Color.GRAY);
        setStroke(Color.WHITE);
        setStrokeWidth(1);
        return this;
    }

    /**
     * Set mouse events
     */
    void SetEvent() {
        setOnMouseClicked(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (!click) {
                    setFill(Color.RED);
                    current_value = 1;
                    click = true;
                } else {
                    setFill(Color.GRAY);
                    current_value = 2;
                    click = false;
                }
            }
            if (event.getButton().equals(MouseButton.MIDDLE)) {
                if (!click) {
                    setFill(Color.GRAY);
                    current_value = 2;
                    click = true;
                } else {
                    setFill(Color.GRAY);
                    current_value = 2;
                    click = false;
                }
            }
            if (event.getButton().equals(MouseButton.SECONDARY)) {
                if (!click) {
                    setFill(Color.WHITE);
                    current_value = 0;
                    click = true;
                } else {
                    setFill(Color.GRAY);
                    current_value = 2;
                    click = false;
                }
            }

        });

    }


}


