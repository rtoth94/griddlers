import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * This Class is responsible for displaying the buttons and the icon on the left layout panel
 */
class LeftLayout extends VBox {

    static Button Hint_Button;
    static Button Finalize;
    static Button Restart;
    static Button SelectMap;
    static Button Save;
    static Button Load;

    private int Bwidth = 110;
    private int Bheight = 50;

    LeftLayout() {
        setStyle("-fx-background-color: #1e61cc");
        setPrefSize(140, 600);
        setSpacing(20);
        setPadding(new Insets(5, 10, 10, 30));

        DrawImage();
        DrawButtons();

    }

    private void DrawButtons() {
        Hint_Button = new Button("Hint");
        Hint_Button.setPrefSize(Bwidth, Bheight);
        Hint_Button.setStyle("-fx-focus-color: transparent;");

        Finalize = new Button("Solve");
        Finalize.setPrefSize(Bwidth, Bheight);
        Finalize.setStyle("-fx-focus-color: transparent;");

        Restart = new Button("Restart");
        Restart.setPrefSize(Bwidth, Bheight);
        Restart.setStyle("-fx-focus-color: transparent;");


        SelectMap = new Button("LoadMap");
        SelectMap.setPrefSize(Bwidth, Bheight);
        SelectMap.setStyle("-fx-focus-color: transparent;");

        Save = new Button("Save");
        Save.setPrefSize(Bwidth, Bheight);
        Save.setStyle("-fx-focus-color: transparent;");

        Load = new Button("Load");
        Load.setPrefSize(Bwidth, Bheight);
        Load.setStyle("-fx-focus-color: transparent;");


        getChildren().addAll(Hint_Button, Finalize, Restart, SelectMap, Save, Load);


    }

    /**
     * Draw out an icon
     */
    private void DrawImage() {
        Image image = new Image("images.jpg");
        ImageView iv1 = new ImageView();
        iv1.setImage(image);
        iv1.setImage(image);
        iv1.setFitWidth(100);
        iv1.setPreserveRatio(true);
        iv1.setSmooth(true);
        iv1.setCache(true);

        getChildren().add(iv1);
    }


}
