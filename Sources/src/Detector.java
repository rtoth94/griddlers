import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is responsible for detecting the numbers in each row and column of Squares, which must be painted.
 * It uses regular expression searching, to get the groups of Squares.
 */
class Detector {

    private Square[][] Object_map;
    private char[][] charmap;
    private Map<Integer, ArrayList<Integer>> row_numbers = new HashMap<>();
    private Map<Integer, ArrayList<Integer>> column_numbers = new HashMap<>();
    private ArrayList<String> rows_undetected = new ArrayList<>();
    private ArrayList<String> cols_undetected = new ArrayList<>();



    /**
     *
     * @param p Filepath to read in.
     * @throws IOException file doesnt!t exists
     */
    Detector(String p) throws IOException {

        FileRead readfile = new FileRead(p);
        charmap = readfile.GetCharMap();

        Object_map = new Square[readfile.outerLength][readfile.innerLength];
        createObjectmap();
        DetectLines();
        DetectColumns();
    }

    /**
     * Create a 2D array with Square objects, and store its current and final value.
     */
    private void createObjectmap() {
        for (int i = 0; i < charmap.length; i++) {
            for (int j = 0; j < charmap[i].length; j++) {
                if (charmap[i][j] == '.') {
                    Object_map[i][j] = new Square(i, j, 0, 0);
                }
                if (charmap[i][j] == 'X') {
                    Object_map[i][j] = new Square(i, j, 1, 1);
                }
            }
        }

    }


    /**
     * Creates a list of strings, as lines
     */
    private void DetectLines() {
        String res = "";
        for (int i = 0; i < charmap.length; i++) {
            for (int j = 0; j < charmap[i].length; j++) {
                char item = charmap[i][j];
                res += item;
            }
            rows_undetected.add(res);
            res = "";

        }

        Detect(rows_undetected, row_numbers);
    }


    /**
     * Creates a list of strings, as columns
     */
    private void DetectColumns() {
        String res = "";
        for (int col = 0; col < charmap[0].length; col++) {
            for (int row = 0; row < charmap.length; row++) {
                char item = charmap[row][col];
                res += item;
            }
            cols_undetected.add(res);
            res = "";
        }

        Detect(cols_undetected, column_numbers);
    }

    /**
     * Detector algorythm. Finds the group of "X" in a string, and it's length puts into map
     * @param source List of lines or List of columns as one string.
     * @param target Row/Col index and the detected values
     */
    private void Detect(ArrayList<String> source, Map<Integer, ArrayList<Integer>> target) {
        int index = 0;
        ArrayList<Integer> list;
        for (String line : source) {
            list = new ArrayList<>();
            Matcher m = Pattern.compile("X+").matcher(line);
            while (m.find()) {
                list.add(m.group(0).length());
            }

            if (list.size() == 0) list.add(0);
            target.put(index, list);
            index++;
        }
    }


    Square[][] SendObjectmap() {
        return Object_map;
    }

    Map<Integer, ArrayList<Integer>> SendRowmap() {
        return row_numbers;
    }

    Map<Integer, ArrayList<Integer>> SendColmap() {
        return column_numbers;
    }
}



