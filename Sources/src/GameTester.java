import javafx.scene.shape.Rectangle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class GameTester extends Game {

    public Square[][] objectmap;
    private String mpath = "map.txt"; //default

    public GameTester(String placeholder) throws IOException {
        super(placeholder);
        super.ReRead(mpath);

        objectmap = super.GetObjectmap();
        DrawStart();
    }

    @Override
    public void DrawStart() {
        for (int row = 0; row < objectmap.length; row++) {
            for (int col = 0; col < objectmap[row].length; col++) {
                objectmap[row][col].current_value = 2;
                objectmap[row][col].click = false;
            }
        }
    }

    @Override
    public void RevealHint()
    {

    }

    @Override
    public void Solve()
    {
        for (int row = 0; row < objectmap.length; row++) {
            for (int col = 0; col < objectmap[row].length; col++)
            {
                objectmap[row][col].current_value = objectmap[row][col].final_value;
            }
        }
    }

    public void RevealGoodColor(Square s)
    {
        s.current_value = s.final_value;
        s.setFill(s.ColorSelector(false));
    }

    public Square GetElementFromPos(ArrayList<Square> container, int index)
    {
        return  container.get(index);
    }

    public int GetRandomIndexFromPos(int range)
    {
        Random rnd = new Random();
        return rnd.nextInt(range);
    }

    public boolean isGood(int row, int col)
    {
        return GetSquare(row, col).current_value == GetSquare(row, col).final_value;
    }

    public Square GetSquare(int row, int col)
    {
        return objectmap[row][col];
    }

    public ArrayList<Square> GetNotGoodSquares()
    {
        ArrayList<Square> possibilities = new ArrayList<>();
        for (int row = 0; row < objectmap.length; row++)
        {
            for (int col = 0; col < objectmap[row].length; col++)
            {
                if(GetSquare(row, col).final_value == 1) {
                    if (!isGood(row, col))
                    {
                        possibilities.add(GetSquare(row, col));
                    }
                }
            }
        }
        return possibilities;
    }
}
