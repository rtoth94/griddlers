import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;


/**
 * This class is responsible for a the bottom layout. There is a Timer text and a
 */

class BottomLayout extends HBox {

    private static Label timer;
    private static Label message;


    /**
     * Set size to default 800 * 600, set the default color, set the padding and draw out timer text and feedback text.
     */
    BottomLayout() {

        setPrefSize(800, 30);
        setStyle("-fx-background-color: #1e61cc");

        setPadding(new Insets(5, 20, 5, 20));

        timer = DrawTimer();
        message = DrawMessage();
    }

    /**
     * Create the timer text
     *
     * @return Label to display the Elapsed time.
     */
    private Label DrawTimer() {
        Label xx = new Label();
        xx.getStyleClass().add("Timer");
        xx.setText("Elapsed Time: ");
        getChildren().add(xx);
        return xx;
    }


    /**
     * Set the time in the text
     *
     * @param m minutes
     * @param s seconds
     */
    static void setText(int m, int s) {
        timer.setText("Elapsed Time: " + Integer.toString(m) + ":" + Integer.toString(s));
    }


    /**
     * Set the feedback message
     *
     * @param m Text of the message to display
     */
    static void setMessage(String m) {
        message.setText(m);
    }


    /**
     * Change the feedback message to an empy String.
     */
    static void dismissMessage() {
        message.setText(" ");
    }


    /**
     * Creates the feedback message Text object.
     *
     * @return Text object to draw.
     */
    private Label DrawMessage() {
        Label xx = new Label();
        xx.getStyleClass().add("Message");
        xx.setText("");
        xx.setTranslateX(200);
        getChildren().add(xx);
        return xx;
    }


}
