
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import java.io.*;
import java.util.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.*;
import javafx.util.Duration;

/**
 * This class is responsible for game itself, with all functionalities
 */
public class Game extends GridPane implements Runnable {


    //############################################################################
    private String mpath = "map.txt"; //default
    private Square[][] objectmap;
    private Map<Integer, ArrayList<Integer>> rowmap;
    private Map<Integer, ArrayList<Integer>> colmap;

    //############################################################################
    private final int off = 5;
    private final String backgroundcolor = "#1e61cc";
    private ArrayList<Square> unrevealed_container = new ArrayList<>();
    private int sec = -1;
    private int min = 0;
    private int NumX;
    private int NumY;
    private double nbx;
    private double nby;
    private volatile boolean win = false;
    private Stage ss = GUI.getStage();
    static ArrayList<File> SaveContainer;
    private Thread t;


    Game() throws IOException {

        SaveContainer = new ArrayList<>();

        resizeListener();

        t = new Thread(this);
        t.start();

        ReRead(mpath);
        Setup();

        EndControl();

    }

    public Game(String placeholder)
    {

    }

    public void ReRead(String ph) throws IOException {
        Detector files = new Detector(ph);
        objectmap = files.SendObjectmap();
        rowmap = files.SendRowmap();
        colmap = files.SendColmap();
        NumX = objectmap[0].length + 2 * off;
        NumY = objectmap.length + 2 * off;

    }

    private void DrawEmptyGrid() {
        for (int i = 0; i < objectmap.length; i++) {
            for (int j = 0; j < off; j++) {
                Label lab = new Label();
                lab.setPrefSize(25, 25);
                lab.setStyle("-fx-background-color: " + backgroundcolor + ";");
                setRowIndex(lab, i + off);
                setColumnIndex(lab, j);
                getChildren().addAll(lab);
            }
        }

        for (int i = 0; i < off; i++) {
            for (int j = 0; j < objectmap[0].length; j++) {
                Label lab = new Label();
                lab.setPrefSize(25, 25);
                lab.setStyle("-fx-background-color: " + backgroundcolor + ";");
                setRowIndex(lab, i);
                setColumnIndex(lab, j + off);
                getChildren().addAll(lab);
            }
        }

    }

    protected void DrawStart() {
        getChildren().clear();
        for (int row = 0; row < objectmap.length; row++) {
            for (int col = 0; col < objectmap[row].length; col++) {
                objectmap[row][col].current_value = 2;
                objectmap[row][col].click = false;
                objectmap[row][col].SetEvent();
                Rectangle rec = objectmap[row][col].DrawS();
                setRowIndex(rec, row + off);
                setColumnIndex(rec, col + off);
                getChildren().addAll(rec);
            }
        }
    }

    private void DrawLoaded() {
        getChildren().clear();
        for (int row = 0; row < objectmap.length; row++) {
            for (int col = 0; col < objectmap[row].length; col++) {
                Rectangle rec = objectmap[row][col].DrawLoaded();
                setRowIndex(rec, row + off);
                setColumnIndex(rec, col + off);
                getChildren().addAll(rec);
            }
        }
    }

    private void DrawNumbers() {
        for (int i = 0; i < rowmap.size(); i++) {
            int ind = 4;
            for (int j = rowmap.get(i).size() - 1; j >= 0; j--) {
                ArrayList<Integer> list = rowmap.get(i);
                String s = Integer.toString(list.get(j));
                Node n = getNodeFromGridPane(this, ind, i + off);
                EditLabel(n, s);
                ind--;

            }
        }
        // VERTICAL
        for (int i = 0; i < colmap.size(); i++) {
            int ind = 4;
            for (int j = colmap.get(i).size() - 1; j >= 0; j--) {
                ArrayList<Integer> list = colmap.get(i);
                String s = Integer.toString(list.get(j));
                Node n = getNodeFromGridPane(this, i + off, ind);
                EditLabel(n, s);
                ind--;
            }
        }


    }

    private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
                return node;
            }
        }
        return null;
    }

    private void Setup() {

        BottomLayout.dismissMessage();

        win = false;

        getChildren().clear();

        sec = -1;
        min = 0;

        ss.setWidth((NumX * 25) + 200);
        ss.setHeight((NumY * 25) + 200);


        setStyle("-fx-background-color: " + backgroundcolor + ";");
        setPadding(new Insets(20, 40, 40, 20));
        DrawStart();
        DrawEmptyGrid();
        DrawNumbers();
        SetButtonfunctions();


        Resize(nbx, nby);


    }

    private void LoadIn() {
        getChildren().clear();
        sec = -1;
        min = 0;


        ss.setWidth((NumX * 25) + 200);
        ss.setHeight((NumY * 25) + 200);

        setStyle("-fx-background-color: " + backgroundcolor + ";");
        setPadding(new Insets(20, 40, 40, 20));
        DrawLoaded();
        DrawEmptyGrid();
        DrawNumbers();
        SetButtonfunctions();

        Resize(nbx, nby);
    }

    private void EditLabel(Node n, String s) {
        if (n.getClass().getName().equals("javafx.scene.control.Label")) {
            Label xx = (Label) n;
            xx.setAlignment(Pos.CENTER);
            xx.setText(s);
            xx.setFont(new Font("Arial", 20));
            xx.setTextFill(Color.WHITE);
            xx.setOnMouseClicked(event -> LabelCheck(xx));
        }
    }

    protected void RevealHint() {
        unrevealed_container.clear();
        for (int row = 0; row < objectmap.length; row++) {
            for (int col = 0; col < objectmap[row].length; col++) {
                if (objectmap[row][col].final_value == 1) {
                    if (objectmap[row][col].current_value != objectmap[row][col].final_value) {
                        unrevealed_container.add(objectmap[row][col]);
                    }
                }
            }
        }
        //Container of possibilities Filled

        if (unrevealed_container.size() == 0) return;

        Random rnd = new Random();
        int index = rnd.nextInt(unrevealed_container.size());
        Square s = unrevealed_container.get(index);
        s.current_value = s.final_value;
        s.setFill(s.ColorSelector(false));
    }

    public Square[][] GetObjectmap()
    {
        return  objectmap;
    }

    private void SetButtonfunctions() {
        LeftLayout.Hint_Button.setOnMouseClicked(event -> RevealHint());
        LeftLayout.Finalize.setOnMouseClicked(event -> Solve());
        LeftLayout.Restart.setOnMouseClicked(event -> Setup());
        LeftLayout.SelectMap.setOnMouseClicked(event -> {
            try {
                LoadMap();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        //################################################################
        LeftLayout.Save.setOnMouseClicked((MouseEvent event) -> {
            try {
                Save_object s = new Save_object(CreateStatemap(), sec, min, mpath);
                BottomLayout.setMessage("Game Saved!");
                DismissMessage();
                SaveContainer.add(new File(mpath + "_Save"));

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        LeftLayout.Load.setOnMouseClicked(event -> {
            try {
                LoadSaved();

            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    private Integer[][] CreateStatemap() {
        Integer[][] statemap = new Integer[objectmap.length][objectmap[0].length];
        for (int row = 0; row < statemap.length; row++) {
            for (int col = 0; col < statemap[row].length; col++) {
                statemap[row][col] = objectmap[row][col].current_value;
            }
        }
        return statemap;
    }

    private void DecodeStatemap(Integer[][] intmap) {


        for (int row = 0; row < intmap.length; row++) {
            for (int col = 0; col < intmap[row].length; col++) {
                objectmap[row][col].current_value = intmap[row][col];
            }
        }

    }

    private void LoadMap() throws IOException {
        sec = -1;
        min = 0;

        // opened directory
        File file = new File("./" + mpath);
        FileChooser fileChooser = new FileChooser();
        if (file != null) {
            File existDirectory = file.getParentFile();
            fileChooser.setInitialDirectory(existDirectory);
        }

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("txt files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        //display
        fileChooser.setTitle("Open Resource File");
        File rf = fileChooser.showOpenDialog(ss);


        if (rf != null) {
            ReRead(rf.getName());
            mpath = rf.getName();
            Setup();
        }


    }

    private void LoadSaved() throws Exception {

        File file = new File("./" + mpath);
        FileChooser fileChooser = new FileChooser();
        if (file != null) {
            File existDirectory = file.getParentFile();
            fileChooser.setInitialDirectory(existDirectory);
        }

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("txt_Save files (*.txt_Save)", "*.txt_Save");
        fileChooser.getExtensionFilters().add(extFilter);

        //display
        fileChooser.setTitle("Open Saved File");
        File rf = fileChooser.showOpenDialog(ss);


        if (rf != null) {
            Save_object loaded = Save_object.load(rf.getName());

            String used_map = loaded.sname.substring(0, rf.getName().length() - 5);
            ReRead(used_map);

            DecodeStatemap(loaded.om);
            mpath = used_map;
            LoadIn();
            sec = loaded.s;
            min = loaded.m;
            BottomLayout.setMessage("Game Loaded!");
            DismissMessage();
        }


    }

    public void Solve() {

        getChildren().clear();

        DrawEmptyGrid();
        DrawNumbers();


        for (int row = 0; row < objectmap.length; row++) {
            for (int col = 0; col < objectmap[row].length; col++) {
                Rectangle rec = objectmap[row][col].DrawFinal();
                setRowIndex(rec, row + off);
                setColumnIndex(rec, col + off);
                getChildren().addAll(rec);
            }
        }

        Resize(nbx, nby);

    }

    @Override
    public void run() {

        while (true) {
            System.out.print(" ");
            if (!win) {
                try {
                    sec += 1;
                    Thread.sleep(1000);

                    if (sec == 59) {
                        min++;
                        sec = -1;
                    }

                    Platform.runLater(this::Change);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void Change() {
        BottomLayout.setText(min, sec);
    }

    private void resizeListener() {
        this.layoutBoundsProperty().addListener(
                (observable, oldBounds, newBounds) -> {
                    double cellHeight = newBounds.getHeight() / NumY;
                    double cellWidth = newBounds.getWidth() / NumX;
                    nbx = cellHeight;
                    nby = cellWidth;
                    for (final Node child : this.getChildren()) {
                        if (child instanceof Square) {
                            ((Square) child).setHeight(cellHeight);
                            ((Square) child).setWidth(cellWidth);
                        }
                        if (child instanceof Label) {
                            ((Label) child).setPrefWidth(cellWidth);
                            ((Label) child).setPrefHeight(cellHeight);

                        }
                    }
                });
    }

    private void Resize(double newBoundsX, double newBoundsY) {

        double cellHeight = newBoundsX;
        double cellWidth = newBoundsY;
        for (final Node child : this.getChildren()) {
            if (child instanceof Square) {
                ((Square) child).setHeight(cellHeight);
                ((Square) child).setWidth(cellWidth);
            }
            if (child instanceof Label) {
                ((Label) child).setPrefWidth(cellWidth);
                ((Label) child).setPrefHeight(cellHeight);

            }

        }
    }

    private void LabelCheck(Label xx) {
        if (Paint.valueOf("Yellow") == xx.getTextFill()) {
            xx.setTextFill(Paint.valueOf("White"));
            xx.setStyle("-fx-font-weight: normal");
        } else {
            xx.setTextFill(Paint.valueOf("Yellow"));
            xx.setStyle("-fx-font-weight: bold");
        }
    }

    private boolean controll() {
        for (int row = 0; row < objectmap.length; row++) {
            for (int col = 0; col < objectmap[row].length; col++) {
                if (objectmap[row][col].final_value == 1) {
                    if (objectmap[row][col].current_value == 1) {
                    } else {
                        return false;
                    }
                } else if (objectmap[row][col].final_value != 1) {
                    if (objectmap[row][col].current_value == 1) {
                        return false;
                    }
                }
            }

        }
        return true;
    }

    private void EndControl() {

        this.setOnMouseClicked(event -> {
            if (!win) {
                if (controll()) {
                    final Stage dialog = new Stage();
                    dialog.initModality(Modality.APPLICATION_MODAL);
                    dialog.initOwner(ss);
                    dialog.setResizable(false);

                    Text l = new Text("You Win!");
                    l.setFill(Paint.valueOf("Red"));
                    l.setFont(Font.font("Impact", 50));
                    l.setTranslateX(70);
                    l.setTranslateY(50);

                    VBox dialogVbox = new VBox(50);
                    dialogVbox.setStyle("-fx-background-color: #1e61cc");
                    dialogVbox.getChildren().add(l);

                    Scene dialogScene = new Scene(dialogVbox, 300, 200);
                    dialog.setScene(dialogScene);
                    dialog.show();


                    win = true;

                    for (int row = 0; row < objectmap.length; row++) {
                        for (int col = 0; col < objectmap[row].length; col++) {
                            objectmap[row][col].setOnMouseClicked(null);
                        }
                    }

                }
            }
        });
    }


    private void DismissMessage() {
        Timeline animation = new Timeline(new KeyFrame(Duration.millis(3500),
                e -> BottomLayout.dismissMessage()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();
    }


}

/**
 * Save the current values and time and opened map path
 */
class Save_object implements Serializable {
    Integer[][] om;
    Integer s;
    Integer m;
    String sname;

    Save_object(Integer[][] objectm, Integer sec, Integer min, String name) throws Exception {
        s = sec;
        m = min;
        om = objectm;
        sname = name + "_Save";


        save(sname);

    }


    private void save(String n) throws Exception {
        ObjectOutputStream fs = new ObjectOutputStream(new FileOutputStream(n));
        fs.writeObject(this);
        fs.close();
    }


    static Save_object load(String savename) throws Exception {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(savename));
        return (Save_object) is.readObject();
    }


}



