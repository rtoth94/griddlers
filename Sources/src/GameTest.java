import javafx.scene.layout.GridPane;
import java.io.IOException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    GameTester gt;

    GameTest() throws IOException {
       gt = new GameTester("dummystring");
    }
    @org.junit.jupiter.api.Test
    void revealHintRefactored() {
    }

    @org.junit.jupiter.api.Test
    void revealGoodColor() {
    }

    @org.junit.jupiter.api.Test
    void getElementFromPos() {
    }

    @org.junit.jupiter.api.Test
    void getRandomIndexFromPos() {
    }

    @org.junit.jupiter.api.Test
    void isGood() {
    }

    @org.junit.jupiter.api.Test
    void getSquare() {
    }

    @org.junit.jupiter.api.Test
    void getNotGoodSquares() throws IOException {
        ArrayList<Square> possibilities = gt.GetNotGoodSquares();
        assertNotEquals(0,gt.objectmap.length);
        assertNotEquals(0,possibilities.size());
        assertEquals(150, gt.objectmap.length * gt.objectmap[0].length);

        // X numbers
        assertEquals(70, possibilities.size());

        gt.Solve();
        ArrayList<Square> ps = gt.GetNotGoodSquares();
        assertEquals(0, ps.size());

    }
}
