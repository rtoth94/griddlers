import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;


/**
 * This class is responsible for the top layout, which displays the title
 */
class TopLayout extends HBox {


    Text xx;
    Integer offset = 40;

    TopLayout() {
        setStyle("-fx-background-color: #1e61cc");
        setPrefSize(800, 60);


        TitleText();


        this.layoutBoundsProperty().addListener(
                (observable, oldBounds, newBounds) -> {
                    double tHeight = newBounds.getHeight() / 2;
                    double tWidth = newBounds.getWidth() / 2;
                    xx.setTranslateX(tWidth - offset);
                    xx.setTranslateY(tHeight - offset / 2);
                });


    }

    private void TitleText() {
        xx = new Text();
        xx.getStyleClass().add("Title");
        xx.setText("Griddlers");
        xx.setFill(Paint.valueOf("white"));
        xx.setStyle("-fx-line-spacing: 2em;");
        xx.setTranslateX(400 - offset);
        getChildren().add(xx);
    }


}
