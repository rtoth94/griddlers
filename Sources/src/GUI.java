import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.io.File;


/**
 * This is a Main class of GUI
 */
public class GUI extends Application {

    private static Stage stage;


    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        LayoutManager layoutManager = new LayoutManager();
        Scene scene = new Scene(layoutManager);


        scene.widthProperty().addListener(ov -> layoutManager.setW(scene.getWidth()));
        scene.heightProperty().addListener(ov -> layoutManager.setH(scene.getHeight()));

        scene.getStylesheets().add("style.css");

        primaryStage.getIcons().add(new Image("icon.png"));
        primaryStage.setTitle("Griddlers");
        primaryStage.setScene(scene);
        primaryStage.show();


        primaryStage.setOnCloseRequest(e -> {
            //DeleteSaves();
            Platform.exit();
            System.exit(0);
        });


    }


    /**
     * Delete the saved files
     */
    private void DeleteSaves() {

        if (Game.SaveContainer.size() == 0) return;

        for (File ff : Game.SaveContainer) {
            try {
                ff.delete();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    /**
     *
     * @return the stage of further use
     */
    static Stage getStage() {
        return stage;
    }


    public static void main(String[] args) {
        launch(args);
    }


}
